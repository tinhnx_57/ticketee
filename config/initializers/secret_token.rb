# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Ticketee::Application.config.secret_key_base = '8bff2dee640864d503fa2cb9a9db7f55b919ab44fe60591a3f0a81f2938d04018b065cdde13decc2c7afb95134532c7472806dcd345e91fcde51be75ac8458e7'
